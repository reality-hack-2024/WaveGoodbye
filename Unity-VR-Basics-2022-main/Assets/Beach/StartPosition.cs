using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SetPlayerStartPosition : MonoBehaviour
{
    public TeleportationProvider teleportationProvider; // TeleportationProvider 的引用
    public TeleportationAnchor teleportationAnchor; // 传送目标的 TeleportationAnchor 引用

    private IEnumerator Start()
    {
        // 确保 teleportationAnchor 已赋值
        if (teleportationAnchor == null)
        {
            Debug.LogError("Teleportation Anchor is not set");
            yield break;
        }

        // 等待一帧以确保所有系统已初始化
        yield return null;

        TeleportRequest request = new TeleportRequest()
        {
            destinationPosition = teleportationAnchor.transform.position,
            destinationRotation = teleportationAnchor.transform.rotation,
            matchOrientation = MatchOrientation.TargetUpAndForward
        };

        teleportationProvider.QueueTeleportRequest(request);
    }
}
