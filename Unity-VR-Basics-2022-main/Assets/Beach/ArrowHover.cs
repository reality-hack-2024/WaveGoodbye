using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureController : MonoBehaviour
{
    public GameObject arrowObject; // 箭头对象
    public GameObject handController; // 手柄控制器对象

    private void Start()
    {
        // 初始化时隐藏箭头
        arrowObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        // 检测是否是指定的手柄控制器
        if (other.gameObject == handController)
        {
            ShowArrow();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == handController)
        {
            HideArrow();
        }
    }

    private void ShowArrow()
    {
        arrowObject.SetActive(true);
    }

    private void HideArrow()
    {
        arrowObject.SetActive(false);
    }
}

