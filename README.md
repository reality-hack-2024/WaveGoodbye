# WAVE GOODBYE

Our App ‘Wave Goodbye' offers a user-centric experience where individuals can safely release their deepest emotions far away from hem into the ocean or sky.”

### Hardware Required
Headset: VIVE XR Elite 

### Software Dependencies
Unity Version: 2023.1.3f1 

## Shout-Outs
Mentors:
Chris 
Lucy 
Barsa
Brielle 
Lafiya 

To ALLLLL the mentors that have help us soooooo much during the whole hackathon, without you guys, we really cannot pull this of <3
