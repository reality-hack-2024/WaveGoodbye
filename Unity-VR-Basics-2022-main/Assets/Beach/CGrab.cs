using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomGrabInteractable : XRGrabInteractable
{
    private Rigidbody rigidbody;
    private bool wasKinematic;
    private bool isFalling = false;
    private Coroutine setKinematicCoroutine;

    private Collider colliderComponent;  // 添加 Collider 变量

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        wasKinematic = rigidbody.isKinematic;

        colliderComponent = GetComponent<Collider>();  // 获取 Collider 组件
    }

    protected override void OnSelectEntered(XRBaseInteractor interactor)
    {
        base.OnSelectEntered(interactor);
        isFalling = false;
        rigidbody.isKinematic = false;  // 立即设置 Rigidbody 为非 kinematic

        if (colliderComponent != null)  // 禁用 Collider
        {
            colliderComponent.enabled = false;
        }

        if (setKinematicCoroutine != null)
        {
            StopCoroutine(setKinematicCoroutine);
            setKinematicCoroutine = null;
        }
    }

    protected override void OnSelectExited(XRBaseInteractor interactor)
    {
        base.OnSelectExited(interactor);
        isFalling = true;
        setKinematicCoroutine = StartCoroutine(SetKinematicAfterFalling());

        if (colliderComponent != null)  // 启用 Collider
        {
            colliderComponent.enabled = true;
        }
    }

    IEnumerator SetKinematicAfterFalling()
    {
        yield return new WaitForSeconds(1);
        if (isFalling)
        {
            rigidbody.isKinematic = wasKinematic;
            isFalling = false;
        }
    }
}
