using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Sirenix.Serialization;
using UnityEngine.SpatialTracking;


public class RoomManagement : MonoBehaviour
{
    private const string SAVE_FOLDER = "/DesignOptions/";
    private string fullSavePath;
    public List<Furniture> allFurniture;
    public List<CameraData> allCameraViews;
    private Dictionary<string, Dictionary<string, List<FurnitureData>>> savedDesignsByDoctor = new Dictionary<string, Dictionary<string, List<FurnitureData>>>();
    // public event Action OnDesignsLoaded;
    

    // private const string DEFAULT_LAYOUT_PATH = "Assets/RoomTest/Template Layout.dat";


    // public void LoadDefaultLayout()
    // {
    //     if (File.Exists(DEFAULT_LAYOUT_PATH))
    //     {
    //         LoadDesignFromDatFile(DEFAULT_LAYOUT_PATH);
    //     }
    //     else
    //     {
    //         Debug.LogError("Default Layout file not found at " + DEFAULT_LAYOUT_PATH);
    //     }
    // }


    private static readonly string DEFAULT_LAYOUT_PATH;

    static RoomManagement()
    {
        #if UNITY_EDITOR
            DEFAULT_LAYOUT_PATH = "Assets/StreamingAssets/Template.dat";
        #else
            DEFAULT_LAYOUT_PATH = Application.streamingAssetsPath + "/Template.dat";
        #endif

    }

    public void LoadDefaultLayout()
    {
        if (File.Exists(DEFAULT_LAYOUT_PATH))
        {
            LoadDesignFromDatFile(DEFAULT_LAYOUT_PATH);
        }
        else
        {
            Debug.LogError("Default Layout file not found at " + DEFAULT_LAYOUT_PATH);
        }
    }



    private void Awake()
    {
        fullSavePath = Application.dataPath + SAVE_FOLDER;
        if (!Directory.Exists(fullSavePath))
        {
            Directory.CreateDirectory(fullSavePath);
        }
        LoadAllDesigns(); // 确保在Awake中调用此方法
    }

    

    public void SaveCurrentDesign(string doctorName)
    {
        string designName = GetNextDesignNameForDoctor(doctorName);
        List<FurnitureData> currentDesign = new List<FurnitureData>();
        foreach (Furniture furniture in allFurniture)
        {
            FurnitureData furnitureData = new FurnitureData(furniture.gameObject.name, furniture.transform.position, furniture.transform.rotation, furniture.transform.localScale);
            SaveChildrenRecursive(furniture.transform, furnitureData);
            currentDesign.Add(furnitureData);
        }

        if (!savedDesignsByDoctor.ContainsKey(doctorName))
        {
            savedDesignsByDoctor[doctorName] = new Dictionary<string, List<FurnitureData>>();
        }
        savedDesignsByDoctor[doctorName][designName] = currentDesign;

        SaveDesignToFile(doctorName, designName, currentDesign);
    }

    private void SaveChildrenRecursive(Transform furnitureTransform, FurnitureData parentData)
    {
        foreach (Transform child in furnitureTransform)
        {
            var childData = new FurnitureData(child.name, child.position, child.rotation, child.localScale);
            parentData.children.Add(childData);
            SaveChildrenRecursive(child, childData); // 递归处理子项目
        }
    }


    private void SaveDesignToFile(string doctorName, string designName, List<FurnitureData> designData)
    {
        string combinedName = $"{doctorName} {designName}";
        byte[] bytes = SerializationUtility.SerializeValue(designData, DataFormat.Binary);
        File.WriteAllBytes(fullSavePath + combinedName + ".dat", bytes);
    }

    public void LoadDesign(string doctorName, string designName)
    {
        if (!savedDesignsByDoctor.ContainsKey(doctorName) || !savedDesignsByDoctor[doctorName].ContainsKey(designName))
        {
            Debug.LogError($"Design {designName} for doctor {doctorName} not found!");
            return;
        }

        List<FurnitureData> designToLoad = savedDesignsByDoctor[doctorName][designName];
        foreach (FurnitureData furnitureData in savedDesignsByDoctor[doctorName][designName])
        {
            Furniture furniture = FindFurniture(furnitureData);
            if (furniture != null)
            {
                furniture.SetFurnitureData(furnitureData);
            }
        }
    }



    private Furniture FindFurniture(FurnitureData furnitureData)
    {
        // 使用名称查找对应的家具
        foreach (Furniture furniture in allFurniture)
        {
            if (furniture.gameObject.name == furnitureData.name)
            {
                return furniture;
            }
        }
        return null;
    }

    private Transform FindChildTransform(Transform parentTransform, FurnitureData childData)
    {
        // 使用名称查找对应的子项目
        foreach (Transform child in parentTransform)
        {
            if (child.name == childData.name)
            {
                return child;
            }
            Transform found = FindChildTransform(child, childData);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }


    public void DeleteDesignsForDoctor(string doctorName)
    {
        if (savedDesignsByDoctor.ContainsKey(doctorName))
        {
            savedDesignsByDoctor.Remove(doctorName);
            foreach (string file in Directory.GetFiles(fullSavePath, doctorName + " *.dat"))
            {
                File.Delete(file);
            }
        }
    }

    public void DeleteAllDesigns()
    {
        savedDesignsByDoctor.Clear();
        foreach (string file in Directory.GetFiles(fullSavePath))
        {
            File.Delete(file);
        }
    }

    public List<string> GetDesignNamesForDoctor(string doctorName)
    {
        if (savedDesignsByDoctor.ContainsKey(doctorName))
        {
            return new List<string>(savedDesignsByDoctor[doctorName].Keys);
        }
        return new List<string>();
    }

    public void LoadAllDesigns()
    {
        foreach (string file in Directory.GetFiles(fullSavePath, "*.dat"))
        {
            string combinedName = Path.GetFileNameWithoutExtension(file);
            string[] names = combinedName.Split(' ');
            if (names.Length < 3) continue;

            string doctorName = names[0];
            string designName = names[1] + " " + names[2];

            byte[] bytes = File.ReadAllBytes(file);
            List<FurnitureData> designData = SerializationUtility.DeserializeValue<List<FurnitureData>>(bytes, DataFormat.Binary);

            if (!savedDesignsByDoctor.ContainsKey(doctorName))
            {
                savedDesignsByDoctor[doctorName] = new Dictionary<string, List<FurnitureData>>();
            }
            savedDesignsByDoctor[doctorName][designName] = designData;
        }
        Debug.Log($"Loaded designs for {savedDesignsByDoctor.Count} doctors.");

        // OnDesignsLoaded?.Invoke();

    }

    private string GetNextDesignNameForDoctor(string doctorName)
    {
        if (!savedDesignsByDoctor.ContainsKey(doctorName) || savedDesignsByDoctor[doctorName].Count == 0)
        {
            return "Design_Option 1";
        }
        int lastNumber = savedDesignsByDoctor[doctorName].Count;
        return "Design_Option " + (lastNumber + 1);
    }


    // LoadDesignFromDatFile
    public void LoadDesignFromDatFile(string filePath)
    {
        byte[] fileData = File.ReadAllBytes(filePath);

        List<FurnitureData> furnitureDatas = SerializationUtility.DeserializeValue<List<FurnitureData>>(fileData, DataFormat.Binary);

        Debug.Log("Number of Furniture in Scene: " + allFurniture.Count);
        Debug.Log("Number of Furniture in .dat file: " + furnitureDatas.Count);

        if (furnitureDatas.Count != allFurniture.Count)
        {
            Debug.LogError("Mismatch in furniture count for the loaded design!");
            return;
        }

        for (int i = 0; i < allFurniture.Count; i++)
        {
            allFurniture[i].SetFurnitureData(furnitureDatas[i]);
        }
    }




    //Camera View setting

    public void SaveCurrentCameraView(Camera camera)
    {
        CameraData currentView = new CameraData(camera.transform.position, camera.transform.rotation);
        allCameraViews.Add(currentView);
    }

    public void LoadCameraView(int index, Camera camera)
    {
        if (index < 0 || index >= allCameraViews.Count)
        {
            Debug.LogError("Camera view index out of range: " + index);
            return;
        }

        CameraData viewToLoad = allCameraViews[index];

        // 禁用 Tracked Pose Driver
        var trackedPoseDriver = camera.GetComponent<TrackedPoseDriver>();
        if (trackedPoseDriver != null) trackedPoseDriver.enabled = false;

        camera.transform.position = viewToLoad.position;
        camera.transform.rotation = viewToLoad.rotation;
        Debug.Log("Camera position: " + camera.transform.position);
        Debug.Log("Camera rotation: " + camera.transform.rotation);

        // 重新启用 Tracked Pose Driver
        if (trackedPoseDriver != null) trackedPoseDriver.enabled = true;
    }


    public void DeleteAllCameraViews()
    {
        allCameraViews.Clear();
    }
}