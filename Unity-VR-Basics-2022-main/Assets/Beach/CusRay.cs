using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomXRRayInteractor : XRRayInteractor
{
    private XRBaseInteractable targetInteractable = null;

    public override bool CanSelect(XRBaseInteractable interactable)
    {
        if(targetInteractable == null)
        {
            targetInteractable = interactable;
        }

        return base.CanSelect(interactable) && targetInteractable == interactable;
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);
        targetInteractable = null;
    }
}

