using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR;

public class CustomInteractor : XRDirectInteractor
{
    public Color wallHighlightColor = Color.green;

    private XRBaseInteractable currentInteractable;
    private Vector3 initialRelativePosition;
    private float originalY;
    private float originalZ;
    private Collider objectCollider;
    private float initialRelativeRotationY;
    private float rotationSnap = 15f;
    private float rotationY = 0;
    private bool isCeilingObject = false;
    private bool isCabinetObject = false;
    private bool isFloorObject = false;
    private Color originalWallColor;
    //Stiky Note
    private bool gripPressedLastFrame = false;
    public GameObject stickyNotePrefab; 
    private GameObject currentStickyNote;
    private bool isStickyNoteObject = false;
    public Transform notesContainer;



    protected void Update()
    {
        if (currentInteractable != null)
        {

            Vector3 targetPosition = transform.position + initialRelativePosition;

            // Adjust Y position based on type of object
            if (isCeilingObject)
            {
                targetPosition.y = originalY - 0.1f;
            }
            else if (isCabinetObject)
            {
                targetPosition.z = originalZ + 0.1f;
            }
            else if (isStickyNoteObject)
            {
                targetPosition.z = originalZ;
                targetPosition.y = originalY;
            }
            else
            {
                targetPosition.y = originalY + 0.1f;
            }


            // If it's a cabinet, constrain it's XY position
            if (isCabinetObject)
            {
                Collider wallCollider = currentInteractable.GetComponent<CustomCabinetInteractable>().wallCollider;
                Vector3 wallBoundsSize = wallCollider.bounds.size;

                // 判断墙的朝向
                WallDirection wallDirection = DetermineWallDirection(wallBoundsSize);

                Vector3 minPosition = wallCollider.bounds.min;
                Vector3 maxPosition = wallCollider.bounds.max;

                if (wallDirection == WallDirection.Z)
                {
                    // 如果墙面向Z轴，允许在YZ方向上移动
                    targetPosition = new Vector3(
                        transform.position.x, // X保持不变
                        Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y),
                        Mathf.Clamp(targetPosition.z, minPosition.z, maxPosition.z)
                    );
                }
                else
                {
                    // 如果墙面向X或Y轴，允许在XY方向上移动
                    targetPosition = new Vector3(
                        Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x),
                        Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y),
                        transform.position.z // Z保持不变
                    );
                }
                currentInteractable.transform.position = targetPosition;
            }
        

            currentInteractable.transform.position = targetPosition;

            // If it's not a cabinet, rotate it
            if (isFloorObject || isCeilingObject)
            {
                rotationY += transform.rotation.eulerAngles.y - initialRelativeRotationY;
                initialRelativeRotationY = transform.rotation.eulerAngles.y;
                float targetRotationY = Mathf.Round(rotationY / rotationSnap) * rotationSnap;
                currentInteractable.transform.rotation = Quaternion.Euler(0, targetRotationY, 0);
            }

            // if (!(isCeilingObject || isCabinetObject || isFloorObject))
            // {  
            //     // rotationY += transform.rotation.eulerAngles.y - initialRelativeRotationY;
            //     // initialRelativeRotationY = transform.rotation.eulerAngles.y;
            //     // float targetRotationY = rotationY;
            //     // currentInteractable.transform.rotation = Quaternion.Euler(0, targetRotationY, 0);
            // }
            if (isStickyNoteObject)
            {
                currentInteractable.transform.position = targetPosition;
                Debug.Log("See it is Sticky Note");
            }
        }

        if (currentInteractable == null && IsGripPressed())
        {
            if (currentStickyNote == null)
            {
                SpawnStickyNoteAtController();
            }
        }

    }

    private WallDirection DetermineWallDirection(Vector3 wallBoundsSize)
    {
        // 假设最小的边界尺寸对应于墙的朝向
        if (wallBoundsSize.x < wallBoundsSize.y && wallBoundsSize.x < wallBoundsSize.z)
        {
            return WallDirection.X;
        }
        if (wallBoundsSize.y < wallBoundsSize.x && wallBoundsSize.y < wallBoundsSize.z)
        {
            return WallDirection.Y;
        }
        return WallDirection.Z;
    }

    public enum WallDirection { X, Y, Z }


    private bool IsGripPressed()
    {
        var rightHandDevice = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
        if (rightHandDevice.isValid)
        {
            if (rightHandDevice.TryGetFeatureValue(CommonUsages.gripButton, out bool gripPressed))
            {
                if (gripPressed && !gripPressedLastFrame)
                {
                    Debug.Log("Grip pressed");
                    gripPressedLastFrame = true;
                    return true;
                }
                gripPressedLastFrame = gripPressed;
            }
        }
        return false;
    }


    private void SpawnStickyNoteAtController()
    {        

        Debug.Log("Spawning Sticky Note at " + transform.position);
        GameObject spawnedNote = Instantiate(stickyNotePrefab, transform.position, Quaternion.identity);
        spawnedNote.transform.localScale = new Vector3(0.002f, 0.002f, 0.002f); // 设置为小尺寸
        spawnedNote.transform.parent = notesContainer; // 将生成的便签设置为Note Container的子对象

        // 截图
        NoteUI noteUI = spawnedNote.GetComponent<NoteUI>();
        if (noteUI != null)
        {
            //noteUI.SaveScreenshot();
            Debug.Log("Screenshot saved for the spawned Sticky Note.");
        }

    }


    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);


        currentInteractable = args.interactable;

        isCeilingObject = currentInteractable.GetComponent<CustomCeilingInteractable>() != null;
        isCabinetObject = currentInteractable.GetComponent<CustomCabinetInteractable>() != null;
        isFloorObject = currentInteractable.GetComponent<CustomInteractable>() != null;
        isStickyNoteObject = currentInteractable.GetComponent<NoteInteractable>() != null;

        if (isStickyNoteObject)
        {
            Debug.Log("Sticky Note start move");
        }

        initialRelativePosition = currentInteractable.transform.position - transform.position;

        originalY = currentInteractable.transform.position.y;
        originalZ = currentInteractable.transform.position.z;

        objectCollider = currentInteractable.GetComponent<Collider>();
        if (objectCollider != null)
        {
            objectCollider.enabled = false;
        }

        initialRelativeRotationY = transform.rotation.eulerAngles.y;
        rotationY = currentInteractable.transform.rotation.eulerAngles.y;

        if (isCabinetObject)
        {
            Renderer wallRenderer = currentInteractable.GetComponent<CustomCabinetInteractable>().wallCollider.gameObject.GetComponent<Renderer>();
            if (wallRenderer != null)
            {
                originalWallColor = wallRenderer.material.color;
                wallRenderer.material.color = wallHighlightColor;
            }
        } 

    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {

        base.OnSelectExited(args);


        if (currentInteractable != null)
        {
            Vector3 newPosition = currentInteractable.transform.position;

            if (isCeilingObject)
            {
                newPosition.y = originalY;
            }
            else if (isCabinetObject)
            {
                newPosition.z = originalZ;
            }
            else
            {
                newPosition.y = originalY;
            }


            currentInteractable.transform.position = newPosition;

            if (objectCollider != null)
            {
                objectCollider.enabled = true;
            }

            if (isCabinetObject)
            {
                Renderer wallRenderer = currentInteractable.GetComponent<CustomCabinetInteractable>().wallCollider.gameObject.GetComponent<Renderer>();
                if (wallRenderer != null)
                {
                    wallRenderer.material.color = originalWallColor;
                }
            }

            currentInteractable = null;
            isCeilingObject = false;
            isCabinetObject = false;
            isStickyNoteObject = false;
        }
        
    }

    void DisableAllColliders()
    {
        foreach (var collider in FindObjectsOfType<Collider>())
        {
            collider.enabled = false;
        }
    }

    void EnableAllColliders()
    {
        foreach (var collider in FindObjectsOfType<Collider>())
        {
            collider.enabled = true;
        }
    }

}
