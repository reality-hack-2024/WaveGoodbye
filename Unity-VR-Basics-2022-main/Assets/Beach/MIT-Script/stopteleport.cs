using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportRayControl : MonoBehaviour
{
    public XRRayInteractor rayInteractor;
    public XRInteractorLineVisual lineVisual; // 射线的可视化组件

    private LayerMask originalLayerMask; // 用于存储原始的交互层

    private void Start()
    {
        if (rayInteractor != null)
        {
            originalLayerMask = rayInteractor.interactionLayerMask;
            rayInteractor.onSelectEntered.AddListener(HandleSelectEntered);
            rayInteractor.onSelectExited.AddListener(HandleSelectExited);
        }
    }

    private void HandleSelectEntered(XRBaseInteractable interactable)
    {
        // 禁用射线的可视化和交互
        if (lineVisual != null) lineVisual.enabled = false;
        rayInteractor.interactionLayerMask = 0; // 0 表示不与任何层交互
    }

    private void HandleSelectExited(XRBaseInteractable interactable)
    {
        // 启用射线的可视化和交互
        if (lineVisual != null) lineVisual.enabled = true;
        rayInteractor.interactionLayerMask = originalLayerMask;
    }

    private void OnDestroy()
    {
        if (rayInteractor != null)
        {
            rayInteractor.onSelectEntered.RemoveListener(HandleSelectEntered);
            rayInteractor.onSelectExited.RemoveListener(HandleSelectExited);
        }
    }
}
