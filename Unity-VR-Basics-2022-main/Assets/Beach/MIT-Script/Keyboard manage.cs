using UnityEngine;
using System.Collections;
using TMPro;

public class TextFlow : MonoBehaviour
{
    public TMP_Text textDisplay; // 用于显示文本的TMP Text 组件
    public string[] flowTexts; // 存储流程文本的数组
    private int currentIndex = 0; // 当前显示的文本索引
    private bool canChangeText = true; // 是否可以切换文本

    public float displayDuration = 5.0f; // 文本显示持续时间
    private float displayTimer = 0.0f;

    void Start()
    {
        // 初始化文本显示
        DisplayText(flowTexts[currentIndex]);
    }

    void Update()
    {
        // 检查是否可以切换文本
        if (canChangeText)
        {
            // 检测玩家按键
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                // 切换到下一句
                NextText();
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                // 切换到上一句
                PreviousText();
            }
        }

        // 更新文本显示计时器
        if (displayTimer > 0)
        {
            displayTimer -= Time.deltaTime;
            if (displayTimer <= 0)
            {
                // 计时结束后，隐藏文本
                textDisplay.text = "";
            }
        }
    }

    void NextText()
    {
        // 切换到下一句文本
        currentIndex = Mathf.Clamp(currentIndex + 1, 0, flowTexts.Length - 1);
        DisplayText(flowTexts[currentIndex]);
    }

    void PreviousText()
    {
        // 切换到上一句文本
        currentIndex = Mathf.Clamp(currentIndex - 1, 0, flowTexts.Length - 1);
        DisplayText(flowTexts[currentIndex]);
    }

    void DisplayText(string text)
    {
        // 显示文本并启动计时器
        textDisplay.text = text;
        displayTimer = displayDuration;
    }
}
