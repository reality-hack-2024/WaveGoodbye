//-----------------------------------------------------------------------
// <copyright file="ShortNoteEditor.cs" company="quanty bandit">
// Copyright (c) quanty bandit. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
using UnityEditor;
using System;

namespace qb.EditorTools
{
    [CustomEditor(typeof(ShortNote))]
    public class ShortNoteEditor : ShortNoteBaseEditor
    {
        /// <summary>
        /// Static method call by "Tools/Short notes/Add a short note" menu
        /// </summary>
        [MenuItem("Tools/Short notes/Add a short note &n")]
        public static void AddShortNoteInScene()
        {
            ShortNote.TagHelper.AddTag(ShortNoteBase.shortNoteTag);

            GameObject go;
            string sDate = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
            if (Selection.activeGameObject != null)
            {
                go = Selection.activeGameObject;
                var notes = go.GetComponents<ShortNote>();
                if (notes != null && notes.Length >= ShortNoteBase.maxNoteCount)
                {
                    EditorUtility.DisplayDialog("Note limit count for one game object reached!", $"The maximum of {notes.Length} notes are already atached to {go.name} ", "ok");
                    return;
                }
            }
            else
            {
                go = new GameObject($"ShortNote [{sDate}]");
                go.tag = ShortNoteBase.shortNoteTag;
                var view = SceneView.lastActiveSceneView;
                if (view != null)
                {
                    Camera sceneCam = view.camera;
                    Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
                    go.transform.position = spawnPos;
                }
            }

            var shortNote = go.AddComponent<ShortNote>();
            shortNote.displayMode = ShortNoteBase.DisplayMode.Always;
            shortNote.date = sDate;
            Selection.activeGameObject = go;
        }
    }
}
