using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class NoteInteractable : XRBaseInteractable
{
    // This script will serve as a marker for ceiling objects
    public Transform NoteTransform; // This transform will be set in Unity editor
}
