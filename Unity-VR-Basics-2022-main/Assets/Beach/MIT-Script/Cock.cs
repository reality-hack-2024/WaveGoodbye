using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using TMPro;
using UnityEngine.EventSystems;

public class BottleCapInteraction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private XRGrabInteractable grabInteractable;
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    
    public TextMeshProUGUI hintText; // TMP Text 元素引用
    public string hoverMessage = "Hover Text Here"; // 鼠标悬停时显示的文本

    private void Start()
    {
        grabInteractable = GetComponent<XRGrabInteractable>();
        grabInteractable.onSelectEntered.AddListener(StartInteraction);
        grabInteractable.onSelectExited.AddListener(EndInteraction);

        // 初始时隐藏文本
        hintText.gameObject.SetActive(false);
    }

    private void StartInteraction(XRBaseInteractor interactor)
    {
        // 交互开始时记录瓶盖的当前位置和旋转角度
        originalPosition = transform.position;
        originalRotation = transform.rotation;
    }

    private void EndInteraction(XRBaseInteractor interactor)
    {
        // 交互结束时将瓶盖恢复到原始位置和旋转角度
        transform.position = originalPosition;
        transform.rotation = originalRotation;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // 当鼠标悬停在瓶盖上时触发
        hintText.text = hoverMessage; // 设置显示的文本
        hintText.gameObject.SetActive(true); // 显示文本
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // 当鼠标移开时触发
        hintText.gameObject.SetActive(false); // 隐藏文本
    }
}
