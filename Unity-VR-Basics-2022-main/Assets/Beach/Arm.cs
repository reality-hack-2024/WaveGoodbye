using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class ArmController : MonoBehaviour
{
    [Header("Rotation Settings")]
    public Transform connectedBody; // 这是新添加的变量
    public Vector3 localRotationPoint; // 这是相对于connectedBody的本地位置
    private Vector3 rotationPoint // 使用属性来动态获取rotationPoint的值
    {
        get
        {
            if (connectedBody)
                return connectedBody.TransformPoint(localRotationPoint);
            return localRotationPoint;
        }
    }
    public Vector3 rotationAxis = Vector3.up;
    public float startAngle = 0f;
    public float endAngle = 90f;
    public float rotationSpeed = 10f;


    [Header("Trigger")]
    public Collider interactionCollider;         // 与手柄交互的Collider

    private float currentAngle;
    private bool isRotating = false;
    private int rotationDirection = 1;

    private List<Collider> colliders = new List<Collider>();

    private void Start()
    {
        // 获取场景中的所有Collider
        colliders.AddRange(FindObjectsOfType<Collider>());
        currentAngle = startAngle;
    }

    private void Update()
    {
        if (isRotating)
        {
            // 旋转摇臂
            currentAngle += rotationSpeed * Time.deltaTime * rotationDirection;
            currentAngle = Mathf.Clamp(currentAngle, startAngle, endAngle);
            transform.RotateAround(rotationPoint, rotationAxis, rotationSpeed * Time.deltaTime * rotationDirection);

            // 当旋转到范围的边缘时，反向旋转
            if (currentAngle <= startAngle || currentAngle >= endAngle)
            {
                rotationDirection = -rotationDirection;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == interactionCollider)
        {
            isRotating = !isRotating;

            // 根据是否正在旋转来启用/禁用场景中的所有Collider
            foreach (var collider in colliders)
            {
                collider.enabled = !isRotating;
            }
        }
    }

    private void OnDrawGizmos()
    {
        // 绘制旋转中心
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(rotationPoint, 0.05f);

        // 绘制旋转轴心方向
        Gizmos.color = Color.green;
        Gizmos.DrawLine(rotationPoint, rotationPoint + rotationAxis.normalized * 0.3f); // 2f 控制轴的长度，可以按需要调整

        // 绘制旋转范围的扇形边界和填充颜色
        DrawRotationArc(rotationPoint, rotationAxis, startAngle, endAngle, 0.3f); // 1.5f 控制半径，可以按需要调整
    }

    private void DrawRotationArc(Vector3 center, Vector3 normal, float startAngle, float endAngle, float radius)
    {
        int segments = 30;
        float step = (endAngle - startAngle) / segments;
        Vector3 previousPoint = center + Quaternion.AngleAxis(startAngle, rotationAxis) * Vector3.right * radius;

        // 绘制从旋转轴到扇形的起始和终点的直线
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(center, previousPoint);

        for (int i = 1; i <= segments; i++)
        {
            Vector3 nextPoint = center + Quaternion.AngleAxis(startAngle + step * i, rotationAxis) * Vector3.right * radius;
            Gizmos.DrawLine(previousPoint, nextPoint);
            
            previousPoint = nextPoint;
        }

        // 绘制从旋转轴到扇形的终点的直线
        Gizmos.DrawLine(center, previousPoint);
    }


}
