using UnityEngine;
using System.Collections;

public class StarBottleEffect : MonoBehaviour
{
    public Material normalMaterial; // 正常材质
    public Material glowingMaterial; // 发光材质
    public float flashInterval = 1.0f; // 闪烁间隔

    private Renderer bottleRenderer;
    private bool isFlashing = false;

    void Start()
    {
        bottleRenderer = GetComponent<Renderer>();
        bottleRenderer.sharedMaterial = normalMaterial;
        isFlashing = false; 
    }


    public void StartFlashing()
    {
        Debug.Log("Start flashing");
        if (!isFlashing)
        {
            StartCoroutine(FlashMaterial());
            isFlashing = true;

            GetComponent<Renderer>().sharedMaterial = glowingMaterial;
        }
    }


    IEnumerator FlashMaterial()
    {
        while (true)
        {
            // 切换材质
            if (bottleRenderer.sharedMaterial == normalMaterial)
            {
                bottleRenderer.sharedMaterial = glowingMaterial;
            }
            else
            {
                bottleRenderer.sharedMaterial = normalMaterial;
            }

            // 等待闪烁间隔
            yield return new WaitForSeconds(flashInterval);
        }
    }
}
