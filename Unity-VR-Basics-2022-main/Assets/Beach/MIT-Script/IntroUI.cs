using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject firstBoard; // 第一个UI板块
    public GameObject secondBoard; // 第二个UI板块
    public GameObject playerCamera; // 玩家的相机或头显
    public GameObject bottlePrefab1; // 第一个漂流瓶Prefab
    public GameObject bottlePrefab2; // 第二个漂流瓶Prefab

    // 当点击"开始"按钮时调用
    public void OnStartButtonClicked()
    {
        firstBoard.SetActive(false); // 隐藏第一个UI板块
        secondBoard.SetActive(true); // 显示第二个UI板块
    }

    // 当点击第一个漂流瓶按钮时调用
    public void OnBottle1Clicked()
    {
        SpawnBottleInFrontOfPlayer(bottlePrefab1);
    }

    // 当点击第二个漂流瓶按钮时调用
    public void OnBottle2Clicked()
    {
        SpawnBottleInFrontOfPlayer(bottlePrefab2);
    }

    // 在玩家眼前生成漂流瓶
    private void SpawnBottleInFrontOfPlayer(GameObject bottlePrefab)
    {
        firstBoard.SetActive(false); // 隐藏第一个UI板块
        secondBoard.SetActive(false); // 显示第二个UI板块
        
        // 玩家眼前的位置
        Vector3 spawnPosition = playerCamera.transform.position + playerCamera.transform.forward * 2.0f; // 2.0f是距离，可以调整
        spawnPosition.y += 1.0f; // 调整高度，使其稍微高于眼平线

        // 生成漂流瓶Prefab
        GameObject spawnedBottle = Instantiate(bottlePrefab, spawnPosition, Quaternion.identity);
        // 添加Rigidbody组件以实现自由落体效果
        if (!spawnedBottle.GetComponent<Rigidbody>())
        {
            spawnedBottle.AddComponent<Rigidbody>();
        }
    }
}
