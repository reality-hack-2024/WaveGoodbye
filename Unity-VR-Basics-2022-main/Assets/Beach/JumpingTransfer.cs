using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR; // 导入 XR 命名空间

public class TeleportController : MonoBehaviour
{
    public TeleportationAnchor[] teleportPoints; // 预设的传送位置点数组
    public TeleportationProvider teleportationProvider; // TeleportationProvider 的引用
    private int currentTeleportIndex = 0; // 当前传送点的索引
    private bool readyToTeleport = true; // 准备好进行下一次传送
    private InputDevice rightHandDevice; // 右手控制器的 InputDevice

    private void Start()
    {
        // 获取右手控制器的 InputDevice
        rightHandDevice = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
    }

    private void Update()
    {
        // 确保设备有效
        if (!rightHandDevice.isValid)
        {
            rightHandDevice = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
        }

        Vector2 inputAxis;
        // 获取摇杆输入
        if (rightHandDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis) && inputAxis.magnitude > 0.1f)
        {
            HandleTeleportInput(inputAxis.x);
        }
        else if (inputAxis.magnitude < 0.1f)
        {
            readyToTeleport = true; // 摇杆回到中心位置，可以进行下一次传送
        }

        // 检查键盘输入
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            HandleTeleportInput(1); // 模拟向右移动的输入
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            HandleTeleportInput(-1); // 模拟向左移动的输入
        }
    }

    private void HandleTeleportInput(float inputX)
    {
        if (readyToTeleport)
        {
            if (inputX > 0.1f)
            {
                // 增加索引并循环
                currentTeleportIndex = (currentTeleportIndex + 1) % teleportPoints.Length;
                TeleportTo(currentTeleportIndex);
            }
            else if (inputX < -0.1f)
            {
                // 减少索引并循环
                if (--currentTeleportIndex < 0)
                {
                    currentTeleportIndex = teleportPoints.Length - 1;
                }
                TeleportTo(currentTeleportIndex);
            }

            readyToTeleport = false; // 设置为不再准备好传送
        }
    }

    // 执行传送
    private void TeleportTo(int index)
    {
        if (teleportPoints == null || teleportPoints.Length == 0 || teleportPoints[index] == null)
        {
            Debug.LogError("Teleport Points are not set up or the teleport point is null");
            return;
        }

        TeleportRequest request = new TeleportRequest()
        {
            destinationPosition = teleportPoints[index].transform.position,
            destinationRotation = teleportPoints[index].transform.rotation,
            matchOrientation = MatchOrientation.TargetUpAndForward
        };

        teleportationProvider.QueueTeleportRequest(request);
        currentTeleportIndex = index;
        readyToTeleport = false;
    }
}
