using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class FurnitureData
{
    public string name;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public List<FurnitureData> children; // 用于存储子项目信息

    public FurnitureData(string name, Vector3 pos, Quaternion rot, Vector3 scl)
    {
        this.name = name;
        position = pos;
        rotation = rot;
        scale = scl;
        children = new List<FurnitureData>();
    }
}
