using UnityEngine;

public class Furniture : MonoBehaviour
{
    public FurnitureData GetFurnitureData()
    {
        var data = new FurnitureData(gameObject.name, transform.position, transform.rotation, transform.localScale);
        foreach (Transform child in transform)
        {
            // 处理子 Transform（使用全局坐标）
            data.children.Add(new FurnitureData(child.name, child.position, child.rotation, child.localScale));
        }
        return data;
    }

    public void SetFurnitureData(FurnitureData data)
    {
        transform.position = data.position;
        transform.rotation = data.rotation;
        transform.localScale = data.scale;

        // 递归地设置子项目的数据
        foreach (var childData in data.children)
        {
            Transform childTransform = transform.Find(childData.name);
            if (childTransform != null)
            {
                SetChildDataRecursive(childTransform, childData);
            }
            else
            {
                Debug.LogError("Child transform not found: " + childData.name);
            }
        }
    }

    private void SetChildDataRecursive(Transform childTransform, FurnitureData childData)
    {
        childTransform.position = childData.position;
        childTransform.rotation = childData.rotation;
        childTransform.localScale = childData.scale;

        for (int i = 0; i < childTransform.childCount; i++)
        {
            var childOfChildData = childData.children[i];
            Transform childOfChildTransform = childTransform.GetChild(i);
            SetChildDataRecursive(childOfChildTransform, childOfChildData);
        }
    }

}
