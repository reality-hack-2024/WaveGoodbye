using UnityEngine;

public class ExitGame : MonoBehaviour
{
    private void Update()
    {
        // 检查是否按下了 Esc 键
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // 如果我们在编辑器中，停止播放
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #endif

            // 如果我们在构建的游戏中，退出游戏
            Application.Quit();
        }
    }
}
