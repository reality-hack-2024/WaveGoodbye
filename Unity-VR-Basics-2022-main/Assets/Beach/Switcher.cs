using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections.Generic;

public class Switcher : MonoBehaviour
{
    public GameObject customRightHandController;
    public GameObject xrDirectRightHandController;
    public List<Collider> interactableColliders = new List<Collider>();
    private bool isUsingCustomController = true;

    void Start()
    {
        // 初始情况下，启用自定义控制器，禁用 XR Direct 控制器
        customRightHandController.SetActive(true);
        xrDirectRightHandController.SetActive(false);
        Debug.Log("start");
    }

    // // 添加一个Collider到交互列表
    // public void AddInteractableCollider(Collider collider)
    // {
    //     interactableColliders.Add(collider);
    // }

    // // 移除一个Collider从交互列表
    // public void RemoveInteractableCollider(Collider collider)
    // {
    //     interactableColliders.Remove(collider);
    // }

    // // 检查手柄是否与任何Collider交互
    // private bool IsInteracting()
    // {
    //     Debug.Log("IsInteracting method called.");
    //     foreach (Collider collider in interactableColliders)
    //     {
    //         if (collider.bounds.Intersects(GetComponent<Collider>().bounds))
    //         {
    //             Debug.Log("GetXRCollider");
    //             return true;
    //         }
    //     }
    //     Debug.Log("GetCSCollider");
    //     return false;
    // }

    // // 在与交互列表中的Collider交互时切换 Controller
    // private void OnTriggerEnter(Collider other)
    // {
    //     AddInteractableCollider(other);
    //     Debug.Log("OnTriggerEnter");

    //     if (IsInteracting())
    //     {
    //         Debug.Log("Interacting with interactable objects");

    //         customRightHandController.SetActive(false);
    //         xrDirectRightHandController.SetActive(true);
    //         isUsingCustomController = false;
    //     }
    //     else
    //     {
    //         Debug.Log("Not interacting with interactable objects");

    //         // 切换到 XR Direct Controller
    //         customRightHandController.SetActive(true);
    //         xrDirectRightHandController.SetActive(false);
    //         isUsingCustomController = true;
    //     }
    // }

    // // 当与Collider交互结束时，从列表中移除Collider
    // private void OnTriggerExit(Collider other)
    // {
    //     RemoveInteractableCollider(other);
    //     Debug.Log("OnTriggerExit");

    //     if (IsInteracting())
    //     {
    //         Debug.Log("Still interacting with interactable objects");
    //     }
    //     else
    //     {
    //         Debug.Log("No longer interacting with interactable objects");

    //         // 切换到 XR Direct Controller
    //         customRightHandController.SetActive(false);
    //         xrDirectRightHandController.SetActive(true);
    //         isUsingCustomController = false;
    //     }
    // }
}

