using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System.Text;
using HuggingFace.API;


public class NoteUI : MonoBehaviour
{
    public GameObject notePrefab;
    //public TMP_Text noteText;
    //public Transform playerCamera;
    private Transform playerCamera; 
    private bool isInteracting = false;
    public BoxCollider noteCollider;
    //public GameObject inputFieldGameObject;
    //public TMP_InputField noteInputField;
    private GameObject stickyNote;
    public RawImage imageArea; // 图片区域
    private Texture2D noteImage; // 保存截图的纹理
        // 新增的 TMP_Text 变量
    public TMP_Text newText;
    ///recording part
    public Button startButton;
    public Button stopButton;
    private AudioClip clip;
    private byte[] bytes;
    private bool recording;
    private string lastRecordingPath = null;

    public BottleGrab bottleGrab;


    void Awake()
    {
        if (Camera.main != null)
        {
            playerCamera = Camera.main.transform;
        }
        else
        {
            Debug.LogError("No main camera found. Ensure your camera is tagged as 'MainCamera'.");
        }

        startButton.onClick.AddListener(StartRecording);
        stopButton.onClick.AddListener(StopRecording);
        stopButton.interactable = false;
    }

    void Update()
    {
        //noteText.enableWordWrapping = true;
        UpdateNoteTransformAndCollider();
        UpdateStickyNotePosition();
        if (recording && Microphone.GetPosition(null) >= clip.samples) {
            StopRecording();
        }
    }

    private void UpdateNoteTransformAndCollider()
    {
        Vector3 directionToFace = playerCamera.position - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(directionToFace);

        // 添加的偏移旋转
        Quaternion offsetRotation = Quaternion.Euler(0, 180, 0); // 根据您的预制体调整这些值

        // 应用偏移旋转
        targetRotation *= offsetRotation;

        // 应用最终旋转到笔记
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5f);


        if (isInteracting)
        {
            //ActivateInputField();
        }
        else
        {
            Vector3 smallScale = new Vector3(0.002f, 0.002f, 0.002f);
            transform.localScale = smallScale;
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5f);
            noteCollider.size = new Vector3(0.2f / smallScale.x, 0.2f / smallScale.y, 0.2f / smallScale.z);
        }
    }


    public void UpdateNoteText(string text)
    {
        //noteText.text = text;
                // 使用 newText 而不是 noteInputField
        newText.text = text;
    }

    public string GetNoteText()
    {
        //return noteText.text;
                // 从 newText 获取文本而不是 noteInputField
        return newText.text;
    }


    public void OnFinishButtonClick()
    {
        gameObject.SetActive(false);
        if (bottleGrab != null)
        {
            // 调用BottleGrab中的ShowPaper方法来显示Paper
            bottleGrab.ShowPaper();
        }
        else
        {
            Debug.LogError("BottleGrab script not found");
        }
    }


    ///////////////////////////////////////////////////////////////
    private void StartRecording() {
        // 删除上一次的录音文件
        if (!string.IsNullOrEmpty(lastRecordingPath) && File.Exists(lastRecordingPath)) {
            File.Delete(lastRecordingPath);
            Debug.Log("Deleted previous recording: " + lastRecordingPath);
        }

        // 重置路径
        lastRecordingPath = null;

        newText.color = Color.black;
        newText.text = "Recording...";
        startButton.interactable = false;
        stopButton.interactable = true;
        clip = Microphone.Start(null, false, 10, 44100);
        recording = true;
    }

    private void StopRecording() {
        var position = Microphone.GetPosition(null);
        Microphone.End(null);
        var samples = new float[position * clip.channels];
        clip.GetData(samples, 0);
        bytes = EncodeAsWAV(samples, clip.frequency, clip.channels);
        recording = false;
        SendRecording();
    }

    private void SendRecording() {
        newText.color = Color.grey;
        newText.text = "Loading...";
        stopButton.interactable = false;

        HuggingFaceAPI.AutomaticSpeechRecognition(bytes, response => {
            newText.color = Color.black;
            newText.text = response; // 更新 newText
            SaveRecordingFile();    // 调用保存录音文件的方法
            startButton.interactable = true;
        }, error => {
            newText.color = Color.red;
            newText.text = error;
            startButton.interactable = true;
        });
    }

    private void SaveRecordingFile() {
        string fileName = BuildFileName("Recording", newText.text, "wav");
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
        File.WriteAllBytes(filePath, bytes);
        Debug.Log($"Recording saved as: {filePath}");

        // 记录当前录音文件路径
        lastRecordingPath = filePath;
    }

    private string BuildFileName(string prefix, string textContent, string extension) {
        string dateTime = System.DateTime.Now.ToString("MMddHHmm");
        string safeContent = RemoveInvalidChars(textContent);
        if (safeContent.Length > 100) {
            safeContent = safeContent.Substring(0, 100);
        }
        return $"{dateTime}_{prefix}_{safeContent}.{extension}";
    }



    private byte[] EncodeAsWAV(float[] samples, int frequency, int channels) {
        using (var memoryStream = new MemoryStream(44 + samples.Length * 2)) {
            using (var writer = new BinaryWriter(memoryStream)) {
                writer.Write("RIFF".ToCharArray());
                writer.Write(36 + samples.Length * 2);
                writer.Write("WAVE".ToCharArray());
                writer.Write("fmt ".ToCharArray());
                writer.Write(16);
                writer.Write((ushort)1);
                writer.Write((ushort)channels);
                writer.Write(frequency);
                writer.Write(frequency * channels * 2);
                writer.Write((ushort)(channels * 2));
                writer.Write((ushort)16);
                writer.Write("data".ToCharArray());
                writer.Write(samples.Length * 2);

                foreach (var sample in samples) {
                    writer.Write((short)(sample * short.MaxValue));
                }
            }
            return memoryStream.ToArray();
        }
    }
    //////////////////////////////////////////////////////

    public void DeleteNote()
    {
        Destroy(notePrefab);
    }

    private void UpdateStickyNotePosition()
    {
        if (stickyNote != null)
        {
            stickyNote.transform.position = transform.position;
            Debug.Log("Updating Sticky Note Position");
        }
    }

    // 保存截图
    public void SaveScreenshot()
    {
        StartCoroutine(CaptureScreenshot());
        Debug.Log("Screenshot saved.");
    }

    // 截图协程
    private IEnumerator CaptureScreenshot()
    {
        yield return new WaitForEndOfFrame();

        // 创建一个Texture2D来保存截图
        noteImage = new Texture2D(Screen.width, Screen.height);

        // 读取屏幕像素并存储到纹理中
        noteImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        noteImage.Apply();

        // 将纹理设置到RawImage显示区域
        imageArea.texture = noteImage;
    }

    // 保存截图为文件并返回文件路径
    public string SaveScreenshotAsFile()
    {
        if (noteImage != null)
        {
            // 获取当前日期时间
            string dateTime = System.DateTime.Now.ToString("MMddHHmm");

            // 获取标签文本并进行处理
            string newTextContent = newText.text;
            // 限制长度，避免文件名过长
            if (newTextContent.Length > 100) {
                newTextContent = newTextContent.Substring(0, 100);
            }
            // 移除无效字符
            newTextContent = RemoveInvalidChars(newTextContent);

            // 构建文件名，移除了 "Screenshot_" 前缀
            string fileName = $"{dateTime}_{newTextContent}.png";
            string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
            File.WriteAllBytes(filePath, noteImage.EncodeToPNG());
            return filePath;
        }
        return null;
    }

    // 移除文件名中的无效字符
    private string RemoveInvalidChars(string text)
    {
        string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
        foreach (char invalidChar in invalidChars)
        {
            text = text.Replace(invalidChar.ToString(), "");
        }
        return text;
    }


}  