using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomCabinetInteractable : XRBaseInteractable
{
    public Collider wallCollider; // We change this from Transform to Collider
}
