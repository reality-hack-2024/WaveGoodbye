using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections.Generic;

public class InteractableColliderSwitcher : MonoBehaviour
{
    public GameObject customRightHandController;
    public GameObject xrDirectRightHandController;



    private void OnTriggerEnter(Collider other)
    {
        // 在进入Collider时切换手柄模型
        Debug.Log("change to xr");
        customRightHandController.SetActive(false);
        xrDirectRightHandController.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        // 在离开Collider时切换回默认手柄模型
        Debug.Log("change back custom");
        customRightHandController.SetActive(true);
        xrDirectRightHandController.SetActive(false);
    }


}
