using UnityEngine;

[System.Serializable]
public class CameraData
{
    public Vector3 position;
    public Quaternion rotation;

    public CameraData(Vector3 pos, Quaternion rot)
    {
        position = pos;
        rotation = rot;
    }
}

