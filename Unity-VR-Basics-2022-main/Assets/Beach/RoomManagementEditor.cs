#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using Sirenix.Serialization;

[CustomEditor(typeof(RoomManagement))]
public class RoomManagementEditor : Editor
{
    private string selectedFilePath = "";

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        RoomManagement roomManagement = (RoomManagement)target;

        GUILayout.Space(20);
        GUILayout.Label("Load Design from .dat File", EditorStyles.boldLabel);

        selectedFilePath = EditorGUILayout.TextField("File Path:", selectedFilePath);

        if (GUILayout.Button("Load Design from .dat File"))
        {
            roomManagement.LoadDesignFromDatFile(selectedFilePath);

        }
    }
}

#endif
