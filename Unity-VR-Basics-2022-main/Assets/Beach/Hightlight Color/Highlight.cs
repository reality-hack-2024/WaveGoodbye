using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartInteractive : MonoBehaviour
{
    public GameObject interactiveObject; // 可交互的对象（父级）
    private Outline outlineComponent; // Outline 组件引用

    void Start()
    {
        // 尝试获取 Outline 组件
        outlineComponent = interactiveObject.GetComponent<Outline>();

        // 如果没有找到，则尝试子对象
        if (outlineComponent == null)
        {
            outlineComponent = interactiveObject.GetComponentInChildren<Outline>();
        }

        // 如果找到了 Outline 组件，初始时禁用它
        if (outlineComponent != null)
        {
            outlineComponent.enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // 启用 Outline
        if (outlineComponent != null)
        {
            outlineComponent.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // 禁用 Outline
        if (outlineComponent != null)
        {
            outlineComponent.enabled = false;
        }
    }
}
