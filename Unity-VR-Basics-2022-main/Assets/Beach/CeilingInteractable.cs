using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomCeilingInteractable : XRBaseInteractable
{
    // This script will serve as a marker for ceiling objects
    public Transform ceilingTransform; // This transform will be set in Unity editor
}
