using UnityEngine.XR.Interaction.Toolkit;
using System.Collections;
using UnityEngine;

public class BottleGrab : MonoBehaviour
{
    public GameObject bottleCap; // 引用瓶塞对象
    public GameObject uiSystem; // 指向你的UI系统的引用
    public GameObject bottle; // 引用瓶子对象
    private XRGrabInteractable grabInteractable; // XR抓取交互组件
    public XRBaseInteractable bottleCapInteractable; // 瓶塞的交互组件
    public float distanceFromCamera = 1.0f; // UI 显示在头显前方的距离
    public Vector3 uiScale = new Vector3(0.003f, 0.003f, 0.003f); // UI 的缩放大小

    private bool isBottleFirstInteracted = true; // 标记瓶子是否第一次被交互
    private bool isFirstTimeGrabbed = true; // 标记是否是第一次抓取瓶子

    public float throwForceMultiplier = 10.0f; // 投掷力度乘数
    public float throwSpeedThreshold = 1.5f; // 抛掷速度阈值
    private Vector3 lastInteractorPosition;
    private float handSpeed;

    public float ceilingHeight = 5.0f; // Ceiling area的高度
    public float additionalForceMultiplier = 5.0f; // 星星瓶子额外上升力的乘数

    public GameObject paper; // 引用Paper对象


    void Start()
    {
        grabInteractable = GetComponent<XRGrabInteractable>();
        grabInteractable.onSelectEntered.AddListener(HandleGrab);
        grabInteractable.onSelectExited.AddListener(HandleRelease);
        uiSystem.SetActive(false); // 初始时隐藏UI系统

        if (bottleCapInteractable != null)
        {
            bottleCapInteractable.onSelectEntered.AddListener(HandleBottleCapSelect);
        }

        paper.SetActive(false);


    }


    public void ShowPaper()
    {
        // 在这里处理显示Paper的逻辑，例如将Paper.SetActive(true);
        if (paper != null)
        {
            paper.SetActive(true);
        }
    }

    void Update()
    {                // 检查瓶子的标签是否为 "StarBottle"
        if (bottle.CompareTag("StarBottle"))
        {
            if (bottle != null && bottle.transform.position.y >= ceilingHeight)
            {
                Rigidbody rb = bottle.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.velocity = Vector3.zero;
                    rb.isKinematic = true; // 停止瓶子的所有移动


                    // // 只有星星瓶才执行以下操作
                    // StarBottleEffect starEffect = bottle.GetComponent<StarBottleEffect>();
                    // if (starEffect != null)
                    // {
                    //     starEffect.StartFlashing();
                    // }
                }
            }
        }
    }


    private void HandleGrab(XRBaseInteractor interactor)
    {
        if (isBottleFirstInteracted)
        {
            StartCoroutine(ShowUIAfterDelay(1.0f));
            isBottleFirstInteracted = false;
        }
        lastInteractorPosition = interactor.transform.position;
    }

    private void HandleRelease(XRBaseInteractor interactor)
    {
        if (interactor.gameObject == bottle)
        {
            isBottleFirstInteracted = true;
        }

        if (!isFirstTimeGrabbed)
        {
            handSpeed = (interactor.transform.position - lastInteractorPosition).magnitude / Time.deltaTime;
            if (handSpeed > throwSpeedThreshold)
            {
                ApplyThrowForce(interactor);
            }
        }

        isFirstTimeGrabbed = false;
    }

    IEnumerator ShowUIAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        PositionUIInFrontOfCamera();
        uiSystem.SetActive(true);
    }

    private void PositionUIInFrontOfCamera()
    {
        if (Camera.main != null)
        {
            Transform cameraTransform = Camera.main.transform;
            uiSystem.transform.position = cameraTransform.position + cameraTransform.forward * distanceFromCamera;
            Vector3 directionToCamera = cameraTransform.position - uiSystem.transform.position;
            uiSystem.transform.rotation = Quaternion.LookRotation(-directionToCamera);
            uiSystem.transform.localScale = uiScale;
        }
    }

    void HandleBottleCapSelect(XRBaseInteractor interactor)
    {
        if (bottleCap != null && bottleCap.activeSelf)
        {
            Debug.Log("Bottle Cap Triggered");
            if (uiSystem != null)
            {
                StartCoroutine(ShowUIAfterDelay(0.0f));
                Debug.Log("UI Triggered");
            }
        }
    }

    void OnDestroy()
    {
        if (grabInteractable != null)
        {
            grabInteractable.onSelectEntered.RemoveListener(HandleGrab);
            grabInteractable.onSelectExited.RemoveListener(HandleRelease);
        }

        if (bottleCapInteractable != null)
        {
            bottleCapInteractable.onSelectEntered.RemoveListener(HandleBottleCapSelect);
        }
    }

    private void ApplyThrowForce(XRBaseInteractor interactor)
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb != null)
        {
            Vector3 throwDirection = interactor.transform.forward;
            Vector3 throwForce = throwDirection * throwForceMultiplier;

            rb.AddForce(throwForce, ForceMode.Impulse);

            // 如果是星星瓶子，应用额外的上升力
            if (bottle.CompareTag("StarBottle"))
            {
                Vector3 additionalUpwardForce = Vector3.up * additionalForceMultiplier;
                rb.AddForce(additionalUpwardForce, ForceMode.Impulse);

                // 启动星星瓶子的频闪效果
                StarBottleEffect starEffect = bottle.GetComponent<StarBottleEffect>();
                if (starEffect != null)
                {
                    starEffect.StartFlashing();
                }
            }

            // 添加额外的外力，模拟扔瓶子
            Vector3 additionalThrowForce = Vector3.up * 5.0f;
            rb.AddForce(additionalThrowForce, ForceMode.Impulse);
        }
    }



}
