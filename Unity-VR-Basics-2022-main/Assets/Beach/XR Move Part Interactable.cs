using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomXRDirectInteractor : XRDirectInteractor
{
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);
        // 这里可以添加进入选择时的自定义逻辑
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);
        // 当交互结束时，重置物体的速度和角速度
        if (args.interactableObject.transform != null)
        {
            Rigidbody interactableRigidbody = args.interactableObject.transform.GetComponent<Rigidbody>();
            if (interactableRigidbody != null)
            {
                interactableRigidbody.velocity = Vector3.zero;
                interactableRigidbody.angularVelocity = Vector3.zero;
            }
        }
    }
}
