//-----------------------------------------------------------------------
// <copyright file="ShortNote.cs" company="quanty bandit">
// Copyright (c) quanty bandit. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace qb.EditorTools
{
    public class ShortNote :
#if UNITY_EDITOR
ShortNoteBase
#else
        MonoBehaviour
#endif
    {
    }
}
